---
published: true
layout: post
title: La responsabilidad del diseñador gráfico según Massimo Vignelli
categories: diseño Vignelli
---

"En el diseño gráfico, la cuestión de la responsabilidad adquiere una especial importancia como una forma de consciencia económica hacia la solución más adecuada de un problema dado.

Con demasiada frecuencia vemos piezas impresas producidas de manera ostentosa sólo para satisfacer el ego de los diseñadores o los clientes. Es importante encontrar soluciones económicamente apropiadas y que tengan en cuenta todos los aspectos del problema.

Por más obvio que parezca, __este es uno de los temas más ignorados por los diseñadores y los clientes__. La responsabilidad es otra forma de disciplina.

Como diseñadores, tenemos tres niveles de responsabilidad:

* __Con nosotros mismos__, la integridad del proyecto y todas sus partes.
* __Con el cliente__, para resolver el problema de una manera económicamente racional y eficiente.
* __Con el público en general__, el consumidor, el usuario del diseño final.

En cada uno de estos niveles, debemos estar dispuestos a comprometernos a buscar la solución más adecuada; la solución que le sea beneficiosa a todas las partes involucradas.

Al final, el diseño debería sostenerse por si solo, sin excusas, explicaciones ni disculpas.
Debe representar la finalización de un proceso exitoso en toda su belleza.

Una solución responsable."


_Texto tomado del Canon de Vignelli_
