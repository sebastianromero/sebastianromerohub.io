---
layout: post
title: Diseñado por Lella Vignelli
categories: diseño Vignelli libros
published: true
---

Massimo Vignelli publicó un libro en el que repasa la prolífica producción de su esposa, Lella Vignelli. Recorre muchos de sus trabajos que van desde el diseño de moda y joyas hasta interiores y muebles.
![](images/Lella_cover.jpg)
La versión en PDF del libro se encuentra en el [siguiente link](http://www.vignelli.com/Designed_by_Lella.pdf)
