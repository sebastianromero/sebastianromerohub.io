---
published: true
layout: post
categories: fotografia Lego
---

Jugando con los puntos de vista, la percepción del espacio y la escala, [Andrew Whyte](http://www.longexposures.co.uk), un fotógrafo inglés, se propuso mostrar lugares y paisajes, muchos de ellos conocidos por todos, pero de una forma original: a través de los ojos del pequeño muñeco Lego.

![legography.jpg](/images/legography.jpg)

A veces, en la locura y el apuro de todos los días, nos olvidamos de mirar lo que nos rodea. Whyte nos llama la atención sobre éste punto. A veces necesitamos parar y mirar lo que tenemos enfrente.

Constantemente interactuamos con la realidad, pero a través de teléfonos, cámaras, tabletas, computadoras… la lista es larga. Ya estamos tan acostumbrados a estar todo el tiempo siendo mediados por pantallas que a veces nos olvidamos de dejar a un lado el teléfono o la cámara y disfrutar de lo que está frente a nosotros.

Si querés ver la colección de fotos completas podés [entrar aqui](http://www.longexposures.co.uk/legography).